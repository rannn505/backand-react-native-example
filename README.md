backand-react-native-example
===

## To run the app on emulator:
- Clone Project:
```bash
    $ git clone https://github.com/backand/react-native-example.git
    $ cd react-native-example
```   
- Install Dependencies:
```bash
    $ npm install
```  
- Set up your emulator:  
ios: https://facebook.github.io/react-native/docs/getting-started.html#xcode (install xcode)  
android: https://facebook.github.io/react-native/docs/getting-started.html#4-set-up-your-android-virtual-device (install Android Studio and run avd)
- Run the app:
```bash
    $ react-native run-ios 
	$ react-native run-android
```  
- ENJOY! :smile:
